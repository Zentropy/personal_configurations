set shell=/bin/sh

set t_u7=
set t_RV=

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Plugins
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
call plug#begin('~/.vim/plugged')

Plug 'scrooloose/nerdtree' " File browser. Crtl+n to bring up
Plug 'tpope/vim-surround' " Guide: https://github.com/tpope/vim-surround
Plug 'tpope/vim-commentary' " Tl;dr: gcc to comment a line. Guide: https://github.com/tpope/vim-commentary
Plug 'vim-syntastic/syntastic'
Plug 'morhetz/gruvbox'
" Leaving these Rust ones since figures that you might want it
Plug 'rust-lang/rust.vim'
Plug 'racer-rust/vim-racer'

call plug#end()

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Vim Plug Autoinstall
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Colors & Themes
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set background=dark

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Esc Normal/Input mode toggle
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

  " Map it after TermResponse to avoid terminal sending junk characters
  " to our buffer InsertEnter
  augroup normal_input_toggle
    autocmd InsertEnter * nnoremap <Esc> a
    "autocmd BufNewFile * nnoremap <Esc> a
    "autocmd BufRead * nnoremap <Esc> a
    "autocmd BufEnter * nnoremap <Esc> a
  augroup END

"Workaround for having to tie esc rebind to InsertEnter because of buffer garbage
autocmd BufRead,BufNewFile * start

"Make cursor look different in Insert mode"
let &t_SI = "\e[6 q"
let &t_EI = "\e[2 q"

"Make backspace work in normal mode too"
nnoremap <BS> X

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Essentials
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

syntax enable
set encoding=utf8

set number
set wrap
set ai "Auto indent
set si "Smart indent
set autoread
set ruler

filetype plugin on
filetype indent on

set lazyredraw

" Ignore case when searching
set ignorecase

" When searching try to be smart about cases
set smartcase

set hlsearch
set incsearch

" Configure backspace so it acts as it should act
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

" No annoying sound on errors
set noerrorbells
set novisualbell
set t_vb=
set tm=500

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Fast reloading of vimrc configs
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
autocmd! bufwritepost .vimrc source %

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Nerdtree
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
map <C-n> :NERDTreeToggle<CR>
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif "Auto close if last window is nerdtree

noremap i k
noremap h i
noremap k j
noremap j h
