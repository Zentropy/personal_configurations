if status is-interactive
  set fish_function_path /home/kc/.config/fish/functions/*/ $fish_function_path
  starship init fish | source
  set -gx PATH ~/.cargo/bin /usr/bin $PATH
  
end

set fish_greeting

# Show hidden files, but leave out all .git stuff
#set fzf_fd_opts --hidden --exclude=.git
### Tab auto complete nonsense
#bind \t accept-autosuggestion
#bind \t forward-char
bind \t complete
