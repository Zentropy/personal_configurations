#!/bin/bash

# Install packages
sudo zypper -n --gpg-auto-import-keys addrepo https://download.opensuse.org/repositories/shells:fish:release:3/openSUSE_Tumbleweed/shells:fish:release:3.repo
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
sudo zypper -n --gpg-auto-import-keys addrepo https://packages.microsoft.com/yumrepos/vscode vscode
sudo zypper refresh
sudo zypper -n --gpg-auto-import-keys install fish git tmux clang lld fzf fd bat kitty mlocate gnome-keyring code

#update mlocate database
sudo updatedb

#setup home dir
mkdir ~/workspace;
mkdir ~/temp;
rm -rf ~/Documents ~/Video
touch ~/.bash_aliases

# Add kmonad and enable its service in systemd
sudo mkdir /kmonad
sudo chmod -R 777 /kmonad
ln -s "$(pwd)"/KMONAD/kcmain.kbd.linux /kmonad/kcmain.kbd.linux
ln -s "$(pwd)"/KMONAD/kmonad /kmonad/kmonad
chmod -x "$(pwd)"/KMONAD/kmonad.service
sudo ln -s "$(pwd)"/KMONAD/kmonad.service /etc/systemd/system/kmonad.service
sudo ln -s "$(pwd)"/KMONAD/kmonad.service /etc/systemd/system/default.target.wants/kmonad.service
sudo systemctl start kmonad.service
sudo systemctl enable kmonad.service

# Install Rust
curl -sSf https://sh.rustup.rs | sh -s -- --default-toolchain=nightly -y;
# Install Mold linker
sudo zypper -n --gpg-auto-import-keys install mold

# Install fisher plugin for fish shell
/usr/bin/fish -c "curl -sL https://git.io/fisher | source && fisher install jorgebucaran/fisher"
# Add fisher plugins
/usr/bin/fish -c "fisher install PatrickF1/fzf.fish"
/usr/bin/fish -c "fisher install jethrokuan/z"
/usr/bin/fish -c "fisher install jorgebucaran/autopair.fish"
/usr/bin/fish -c "fisher install gazorby/fish-abbreviation-tips"
#Change default shell to fish
chsh -s `which fish`;

# Symlink custom config.fish
rm -f ~/.config/fish/config.fish
ln -s "$(pwd)"/FISH/config.fish ~/.config/fish/config.fish

# Symlink custom fish_variables
rm -f ~/.config/fish/fish_variables
ln -s "$(pwd)"/FISH/fish_variables ~/.config/fish/fish_variables

#Symlink Fish function library
# MUST symlink entire directory!
rm -rf ~/.config/fish/functions
ln -s "$(pwd)"/FISH/functions/ ~/.config/fish/

#Other config files
ln -s "$(pwd)"/TMUX/.tmux.conf ~/.tmux.conf
ln -s "$(pwd)"/VIM/.vimrc ~/.vimrc

# Kitty terminal config
rm -rf ~/.config/kitty/kitty.conf
ln -s "$(pwd)"/KITTY/kitty.conf ~/.config/kitty/kitty.conf

echo "Setup complete! You may run >tide configure< in Fish to configure prompt"


/////////// TEMP DOTBOT CONFIG STORAGE

