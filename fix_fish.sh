#!/bin/bash

# Symlink custom config.fish
rm -f ~/.config/fish/config.fish
ln -s "$(pwd)"/FISH/config.fish ~/.config/fish/config.fish

# Symlink custom fish_variables
rm -f ~/.config/fish/fish_variables
ln -s "$(pwd)"/FISH/fish_variables ~/.config/fish/fish_variables

#Symlink Fish function library
# MUST symlink entire directory!
rm -rf ~/.config/fish/functions
ln -s "$(pwd)"/FISH/functions/ ~/.config/fish/

echo "Setup complete! Please run >tide configure< in Fish to configure prompt"

